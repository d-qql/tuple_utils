//
// Created by petrov on 16.11.23.
//
#include "gtest/gtest.h"
#include <tuple_utils/tuple_utils.hpp>
#include <iostream>

TEST(A, AA) {
    std::tuple<int, int, double, char, int> t{1, 2, 3., 'a', 5};
    auto extracted = tplutils::extract<int, double>(t);

    static_assert(
        std::same_as<decltype(extracted), std::tuple<int, int, double, int>>);
    auto [a, b, c, d] = extracted;
    std::cout << a << " " << b << " " << c << " " << d << std::endl;

    auto uniqueExtracted = tplutils::extract<int>(t);
    auto vec = tplutils::to_vector(uniqueExtracted);
    std::cout << vec[0] << " " << vec[1] << " " << vec[2] << std::endl;
}
