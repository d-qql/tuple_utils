set(HEADERS
        tuple_utils/tuple_utils.hpp)

add_library(${PROJECT_NAME} INTERFACE ${HEADERS})
target_include_directories(${PROJECT_NAME} INTERFACE ${CMAKE_CURRENT_SOURCE_DIR} "${CMAKE_CURRENT_SOURCE_DIR}/..")
target_link_libraries(${PROJECT_NAME} INTERFACE type_pack)
target_compile_features(${PROJECT_NAME} INTERFACE cxx_std_23)
