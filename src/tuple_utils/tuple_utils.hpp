//
// Created by petrov on 16.11.23.
//

#ifndef TUPLE_UTILS_TUPLE_UTILS_HPP
#define TUPLE_UTILS_TUPLE_UTILS_HPP
#include <tuple>
#include <type_pack/parameter_pack_utils.hpp>
#include <type_pack/type_pack.hpp>

#include "tuple_utils.hpp"

namespace tplutils {
    namespace detail {
        template<typename to_extract_tp, typename tuple_t, std::size_t i,
            bool need_to_extract =
                    tp::contains<std::tuple_element_t<i, tuple_t>>(to_extract_tp{})>
        struct extractor {
            static auto process(tuple_t const&) { return std::tuple<>{}; }
        };

        template<typename to_extract_tp, typename tuple_t, std::size_t i>
        struct extractor<to_extract_tp, tuple_t, i, true> {
            static auto process(tuple_t const&t) {
                return std::tuple<std::tuple_element_t<i, tuple_t>>{std::get<i>(t)};
            }
        };

        template<typename to_extract_tp, typename tuple_t, std::size_t... i>
        auto extract_impl(to_extract_tp to_extract, tuple_t const&t,
                          std::index_sequence<i...>) {
            return std::tuple_cat(extractor<to_extract_tp, tuple_t, i>::process(t)...);
        }

        template<typename Tuple, std::size_t... Is>
        auto to_vector_impl(Tuple&&t, std::index_sequence<Is...>) {
            return std::vector<std::tuple_element_t<0, std::decay_t<decltype(t)>>>{
                std::get<Is>(std::forward<Tuple>(t))...
            };
        }
    } // namespace detail

    template<typename... types_to_extract, typename... types>
    auto extract(std::tuple<types...> const&t) {
        return detail::extract_impl(tp::type_pack<types_to_extract...>{}, t,
                                    std::make_index_sequence<sizeof...(types)>());
    }

    template<typename... types>
        requires (tp::same_types<types...>())
    auto to_vector(std::tuple<types...> const&t) {
        return detail::to_vector_impl(t, std::make_index_sequence<sizeof...(types)>{});
    }

    template<typename... types>
        requires (tp::same_types<types...>())
    auto to_vector(std::tuple<types...>&&t) {
        return detail::to_vector_impl(std::move(t), std::make_index_sequence<sizeof...(types)>{});
    }
} // namespace tplutils

#endif // TUPLE_UTILS_TUPLE_UTILS_HPP
